package com.senselessweb.tfmaps.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.senselessweb.tfmaps.TfmapsApplication;
import com.senselessweb.tfmaps.domain.MapModel;

@Service
public class GeneratorService {
  
  private final HeightMapGenerator heightMapGenerator;
  private final LuaMapGenerator luaMapGenerator;
  private final File targetDir;
  
  public GeneratorService(
      final HeightMapGenerator heightMapGenerator,
      final LuaMapGenerator luaMapGenerator,
      final @Qualifier(TfmapsApplication.BEAN_ZIP_DIR) File targetDir) {
    this.heightMapGenerator = heightMapGenerator;
    this.luaMapGenerator = luaMapGenerator;
    this.targetDir = targetDir;
  }
  
  public File generate(final MapModel model) {
    
    final File zip = new File(targetDir, "map" + model.hashCode() + ".zip");
    if (zip.isFile() && zip.length() > 0) {
      return zip;
    }
    
    try {
      final ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip));
      zip(out, heightMapGenerator.generateMap(model), "heightmap.png");
      zip(out, luaMapGenerator.generate(model), "map.lua");
      out.close();
    } catch (final IOException e) {
      throw new RuntimeException(e);
    }
    
    return zip;
  }

  private void zip(ZipOutputStream out, File file, String filename) throws IOException {
    final ZipEntry e = new ZipEntry(filename);
    out.putNextEntry(e);
    try (final FileInputStream input = new FileInputStream(file)) {
      IOUtils.copy(input, out);
    }
    out.closeEntry();
  }
}
