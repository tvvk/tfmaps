package com.senselessweb.tfmaps.web;

import java.io.File;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.senselessweb.tfmaps.domain.MapModel;
import com.senselessweb.tfmaps.generator.GeneratorService;

@RestController
public class GenerateMapController {
  
  public static final class Result {
    
    private final boolean successfull;
    private final String relativeUrl;
    
    public Result(final boolean successfull, final String relativeUrl) {
      this.successfull = successfull;
      this.relativeUrl = relativeUrl;
    }
    
    public boolean isSuccessfull() {
      return successfull;
    }
    
    public String getRelativeUrl() {
      return relativeUrl;
    }
  }
  
  private final GeneratorService generator;
  
  public GenerateMapController(final GeneratorService generator) {
    this.generator = generator;
  }
  
  @RequestMapping(path="generator", method=RequestMethod.POST)
  public Result generate(@RequestBody MapModel model) {
    final File zip = generator.generate(model);
    return new Result(true, ResourceHandlerConfigurer.ZIPURL_PREFIX + zip.getName());
  }
}
