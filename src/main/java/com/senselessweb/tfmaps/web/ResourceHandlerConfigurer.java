package com.senselessweb.tfmaps.web;

import java.io.File;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.senselessweb.tfmaps.TfmapsApplication;

@Component
public class ResourceHandlerConfigurer extends WebMvcConfigurerAdapter {
  
  public static final String ZIPURL_PREFIX = "/zip/";
  private final File zipDir;
  
  public ResourceHandlerConfigurer(final @Qualifier(TfmapsApplication.BEAN_ZIP_DIR) File zipDir) {
    this.zipDir = zipDir;
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler(ZIPURL_PREFIX + "**").addResourceLocations(zipDir.toURI().toString());
  }
}
