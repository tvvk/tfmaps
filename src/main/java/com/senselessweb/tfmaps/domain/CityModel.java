package com.senselessweb.tfmaps.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CityModel extends ObjectModel {

  private final double size;

  public CityModel(
      final @JsonProperty("name") String name, 
      final @JsonProperty("position") LatLng position, 
      final @JsonProperty("size") double size) {
    super(name, position);
    this.size = size;
  }
  
  public double getSize() {
    return size;
  }
  
  @Override
  public boolean equals(Object obj) {
    return EqualsBuilder.reflectionEquals(this, obj, true);
  }
  
  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this, true);
  }

}
