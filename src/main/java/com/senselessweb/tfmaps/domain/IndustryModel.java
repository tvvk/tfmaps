package com.senselessweb.tfmaps.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class IndustryModel extends ObjectModel {

  private final IndustryType type;
  
  @JsonCreator
  public IndustryModel(
      final @JsonProperty("type") IndustryType type, 
      final @JsonProperty("name") String name, 
      final @JsonProperty("position") LatLng position) {
    super(name, position);
    this.type = type;
  }

  public String getIndustryFilename() {
    return type.getFilename();
  }
  
  @Override
  public boolean equals(Object obj) {
    return EqualsBuilder.reflectionEquals(this, obj, true);
  }
  
  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this, true);
  }


}
