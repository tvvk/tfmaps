package com.senselessweb.tfmaps.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LatLng {

  private final double lat;
  private final double lng;
  
  @JsonCreator
  public LatLng(
      final @JsonProperty("lat") double lat, 
      final @JsonProperty("lng") double lng) {
    this.lat = lat;
    this.lng = lng;
  }
  
  public double getLat() {
    return lat;
  }
  
  public double getLng() {
    return lng;
  }
  
  @Override
  public boolean equals(Object obj) {
    return EqualsBuilder.reflectionEquals(this, obj, true);
  }
  
  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this, true);
  }
  
  @Override
  public String toString() {
    return lat + "," + lng;
  }
  
}
