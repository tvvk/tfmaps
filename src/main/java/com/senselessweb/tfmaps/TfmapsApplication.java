package com.senselessweb.tfmaps;

import java.io.File;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TfmapsApplication {

  public static final String BEAN_TMP_DIR = "beanTmpDir";
  public static final String BEAN_ZIP_DIR = "beanZipDir";

  public static void main(String[] args) {
		SpringApplication.run(TfmapsApplication.class, args);
	}
  
  @Bean(BEAN_TMP_DIR)
  public File tmpDir() {
    return makeDir("tfmaps-tmp");
  }
  
  @Bean(BEAN_ZIP_DIR)
  public File zipDir() {
    return makeDir("tfmaps-zip");
  }
  
  private File makeDir(final String name) {
    final File dir  = new File(System.getProperty("java.io.tmpdir"), name);
    if (!dir.isDirectory()) {
      if (!dir.mkdirs()) {
        throw new RuntimeException("Could not create tmp dir");
      }
    }
    return dir;
  }
}
